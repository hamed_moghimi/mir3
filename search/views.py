import json
import os
import shutil
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http.response import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render
from search.crawler import Crawler
from search.funcs import *
# from search.funcs import createIndex
from search.models import Crawl, Index
from ui.funcs import jsonResponse


def crawl_start(request):
    """
    Starts a crawl
    """
    if Crawler.isRunning():
        return jsonResponse({'Error': 1})
    else:
        url = request.POST.get('url') or 'http://en.wikipedia.org/wiki/information_retrieval'
        try:
            max = int(request.POST.get('max'))
        except:
            max = 1
        method = request.POST.get('method') or 'DFS'

        crawler = Crawler(url, max, method)
        Crawler.setInstance(crawler)
        crawler.start()
        #time.sleep(2)

    return jsonResponse({'OK': 1})


def crawl_progress(request):
    """
    Respond progress information about current crawl
    """
    crawler = Crawler.getInstance()
    docs = crawler.docs
    max = crawler.max
    p = 100 * docs // max
    state = crawler.state

    if crawler.state != 'running':
        Crawler.setInstance(None)

    context = {
        'state': state,
        'percent': '{0}%'.format(p),
        'docs': docs,
        }
    return jsonResponse(context)


def clear(request):
    """
    Clears the current dataset
    """
    try:
        # no crawl in progress
        assert Crawler.getInstance() is None or Crawler.getInstance().state != 'running'
        # clear corpus
        shutil.rmtree(Crawler.corpus)
        os.makedirs(Crawler.corpus)
        # clear crawls history
        Crawl.objects.all().delete()
        # respond OK
        return HttpResponse('<p class="text-success">Dataset is now clear.</p>')
    except Exception, e:
        return HttpResponseForbidden('<p class="text-danger">Cannot clear dataset now.</p>')


def index_create(request):
    name = request.POST.get('name')
    stopwords = request.POST.get('stopwords') == '1'
    stemming = request.POST.get('stemming') == '1'
    pagerank = request.POST.get('pagerank') == '1'
    if Index.objects.filter(name = name).exists():
        return HttpResponseForbidden()
    else:
        createIndex(dirc = Crawler.corpus, name = name, pagerank = pagerank, stopwords = stopwords, stemming = stemming)
        selected = not Index.objects.filter(selected = True).exists()
        Index.objects.create(name = name, stopwords = stopwords, stemming = stemming, pagerank = pagerank, selected = selected)
        indices = Index.objects.all()
        return render(request, '_index-list.html', {'indices': indices})

def index_select(request):
    pk = int(request.POST.get('id'))
    Index.objects.update(selected = False)
    Index.objects.filter(pk = pk).update(selected = True)
    indices = Index.objects.all()
    return render(request, '_index-list.html', {'indices': indices})

def search(request):
    """
    Searches the query in the current index and retrieve relevant documents
    """

    index = Index.objects.filter(selected = True)
    if index:
        index = index[0]
    else:
        return render(request, 'result.html', {'error': 'Sorry, Sharif Search is not available right now!'})

    query = request.GET.get('query')
    if query:
        words = query.split(' ')

        # give query to search engine and retrieve ids
        ids = getDocs(index.name, query, index.stemming)

        docs = []
        # representing docs
        for i in ids:
            try:
                with open(Crawler.corpus + i, 'r') as inp:
                    doc = json.load(inp)
            except:
                continue
            docs.append({
                'id': i,
                'title': bold_matches(doc['title'], words),
                'url': doc['url'],
                'summery': bold_matches(doc['summery'], words)
            })

        # pagination
        paginator = Paginator(docs, 10)
        page = request.GET.get('page')
        try:
            docs = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            docs = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            docs = paginator.page(paginator.num_pages)
        pageLink = '?query={0}&page='.format(query)

        # render
        return render(request, 'result.html', {'query': query, 'docs': docs, 'pageLink': pageLink})
    else:
        return HttpResponseRedirect('/')