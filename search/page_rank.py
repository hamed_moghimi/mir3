import numpy
import scipy.linalg


def get_arr(arr, alpha, n, idMap):
	teleport_prob = alpha / n
	def_arr = [teleport_prob] * n
	if len(arr) <= 1:
		return def_arr
	arr = arr[1:]
	def f(x):
		if idMap[x] in arr:
			return (1.0 - alpha) / len(arr)
		return 0.0
	nei_arr = [f(nei) for nei in range(n)]
	return [x + y for x , y in zip(def_arr, nei_arr)]
		

def getIdMap(g):
	res = {}
	i = 0
	for node in g:
		if len(node) > 0:
			res[i] = node[0]
			i += 1
	return res

# I supposed that g is a list of lists, with each of the inner lists being the neighbors of a node, sorted by doc id
def page_rank(g, alpha):
	n = len(g)
	if n == 0:
		return {}
	if n == 1:
		if len(g[0]) < 1:
			return {}
		else:
			return {g[0][0] : 1.0}
	idmap = getIdMap(g)
	cur_arr = numpy.append([get_arr(g[0], alpha, n, idmap)], [get_arr(g[1], alpha, n, idmap)], axis = 0)
	if n > 2:
		for node in g[2:]:
			cur_arr = numpy.append(cur_arr, [get_arr(node, alpha, n, idmap)], axis = 0)
	p = scipy.linalg.eig(cur_arr,left=True,right=False)
	index = numpy.where(p[0] == max(p[0]))[0][0]
	p = p[1][:,index]
	p = p / sum(p)
	res = {}
	for i in range(n):
		res[idmap[i]] = p[i]
	return res
	# return p / sum(p)

if __name__ == "__main__":
	print page_rank([['p0', 'p2'],['p1', 'p1','p2'], ['p2', 'p2', 'p0', 'p3'], ['p3', 'p3','p4'], ['p4', 'p6'], ['p5', 'p5','p6'], ['p6', 'p6','p4','p3']], 0.14)

	


