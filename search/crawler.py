from collections import deque
import json
import os
import re
from threading import Thread
from urllib2 import urlopen
import bs4
from django.conf import settings
from django.utils import timezone
from search.models import Crawl
from search.page_rank import page_rank


class Crawler(Thread):
    """
    A web crawler class
    This class is singleton
    """

    # static variables, Singleton and URL pattern
    _instance = None
    domain = 'http://en.wikipedia.org/wiki/'
    pattern = re.compile(r'^(?:http://en\.wikipedia\.org)?/wiki/(\w+)(#|$)')
    cache = os.path.join(settings.BASE_DIR, 'data/cache/').replace('\\', '/')
    corpus = os.path.join(settings.BASE_DIR, 'data/corpus/').replace('\\', '/')

    @classmethod
    def isRunning(cls):
        return cls._instance is not None and cls._instance.state == 'running'

    @classmethod
    def getInstance(cls):
        return cls._instance

    @classmethod
    def setInstance(cls, instance):
        cls._instance = instance


    # instance methods and variables

    url = None  # first url
    max = None  # max number of docs
    method = None  # traversal method
    docs = 0  # number of docs downloaded till now
    state = None # indicates whether the crawler is running or not
    startTime = None # start date and time
    watched = None
    IDs = {}
    graph = []

    def __init__(self, url, maxDocs, method):
        super(Crawler, self).__init__()
        self.url = url
        self.max = maxDocs
        self.method = 1 if method == 'BFS' else 2

    def createCrawlModel(self):
        crawl = Crawl()
        crawl.docs = self.docs
        crawl.url = self.url
        crawl.method = self.method
        crawl.datetime = self.startTime
        crawl.save()


    def computePageRank(self):
        # g = []
        # for node in self.graph:
        #     if node not in self.IDs:
        #         continue
        #     adj = [self.IDs[node]]
        #     adj.extend(self.graph[node])
        #     g.append(adj)

        alpha = 0.1
        ids = self.IDs.values()
        g = [map(self.IDs.get, filter(lambda i: i in self.watched , x)) for x in self.graph]
        print g
        pagerank = page_rank(g, alpha)
        with open(self.corpus + '.pagerank', 'w') as o:
            for k in pagerank:
                o.write('%s %f\n' % (k, pagerank[k]))

    def run(self):

        watched = set()
        max = self.max
        pattern = self.pattern
        domain = self.domain
        docs = 0

        self.state = 'running'
        self.startTime = timezone.now()
        self.docs = 0
        self.errors = 0

        # initializing frontier
        m = pattern.match(self.url)
        if not m:
            self.state = 'error'
            return
        article = m.group(1)
        frontier = deque([article])
        if self.method == 1:
            # BFS
            pop = frontier.popleft
        else:
            # otherwise, DFS
            pop = frontier.pop

        # doing search
        while frontier and docs < max:

            # get next doc's URL and article name
            article = pop()
            # check if url is watched, because we don't control duplication in frontier for performance
            if article in watched:
                continue

            url = domain + article
            ID = str(docs)
            self.IDs[article] = ID

            try:
                # get doc from the cache or the web
                if os.path.exists(self.cache + article):
                    print 'Reload from cache: %s' % article
                    response = open(self.cache + article, 'r')
                    doc = response.read()
                    response.close()
                else:
                    print 'Fetching data from URL: %s' % article
                    response = urlopen(url)
                    doc = response.read()
                    response.close()

                    # cache the file
                    backup = open(self.cache + article, 'w')
                    backup.write(doc)
                    backup.close()

                # create soup for parsing
                # TODO: recognize main content panel and use it instead of whole html document
                soup = bs4.BeautifulSoup(doc)

                # parsing doc and saving document model
                title = soup.title.string
                text = re.sub(r'(\s)\s*', '\1', soup.get_text())
                hTags = soup.find_all(re.compile(r'h\d'))
                headers = " ".join((h.get_text() for h in hTags))

                # extracting summery
                firstP = soup.find('p')
                summery = firstP.get_text() if firstP else text
                summery = summery[:300] + '...'

                dic = {'title': title, 'text': text, 'id': ID, 'headers': headers, 'url': url, 'summery': summery}
                jsonified = json.dumps(dic)
                with open(self.corpus + str(ID) , 'w') as o:
                    o.write(jsonified)

                # extracting links
                aTags = soup.find_all('a', href=pattern)
                links = set((pattern.match(link.get('href')).group(1) for link in aTags))

                # creating adjacent list for graph
                adj = [article]
                for link in links:
                    if link != article:
                        adj.append(link)
                self.graph.append(adj)

                # add this url to watched and add its links to frontier
                watched.add(article)
                links = filter(lambda link: link not in watched and link not in frontier, links)
                frontier.extend(links)

            except Exception, e:
                print 'Error:', e
                self.errors += 1
                continue

            # this doc parsed successfully
            docs += 1
            self.docs = docs

        # crawl finished
        self.state = 'finished'
        self.watched = watched
        self.computePageRank()
        self.createCrawlModel()