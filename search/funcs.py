import re
import subprocess
import os
from django.conf import settings


def bold_matches(text, words):
    pattern = r'\b({0})\b'.format('|'.join(words))
    regex = re.compile(pattern, re.I)
    return regex.sub(r'<strong>\1</strong>', text)

def createIndex(dirc, name, pagerank, stopwords, stemming):
    pr = "0"
    if pagerank:
        pr = "1"

    st = "0"
    if stopwords:
        st = "1"

    stem = "0"
    if stemming:
        stem = "1"
    option = pr + st + stem
    b = settings.BASE_DIR + '/java/index.jar'
    print subprocess.check_output(['java', '-jar', b, 'index', dirc, name, option])

def getDocs(name, query, stem):
    stemStr = "false"
    if stem:
        stemStr = "true"

    b = settings.BASE_DIR + '/java/index.jar'
    l = subprocess.check_output(['java', '-jar', b, 'search', name, stemStr, '1', '1000', '"%s"' % query])
    lis = l.strip().split(' ')
    return lis
    #return filter(lambda x: x != '0', lis)