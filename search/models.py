from django.contrib import admin
from django.db import models

# Create your models here.
TRAVERSAL_METHODS = ((1, 'BFS'), (2, 'DFS'))
YES_NO = ((True, 'Yes'), (False, 'No'))

class Crawl(models.Model):
    """
    Model for every crawl in web. Date and time, traversal method, max number of docs and ...
    """
    docs = models.IntegerField()
    datetime = models.DateTimeField()
    method = models.SmallIntegerField(choices=TRAVERSAL_METHODS)
    url = models.CharField(max_length=100)

    def __unicode__(self):
        return "%s, %s, %d docs" % (self.datetime.strftime('%y/%m/%d - %H:%M'), self.get_method_display(), self.docs)


class Index(models.Model):
    """
    Model for index. Index options such as page rank, stemming and stop words elimination
    """
    name = models.CharField(max_length=30, unique=True)
    stopwords = models.BooleanField(choices=YES_NO)
    stemming = models.BooleanField(choices=YES_NO)
    pagerank = models.BooleanField(choices=YES_NO)
    selected = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name


admin.site.register(Crawl)
admin.site.register(Index)