"use strict";

$(function(){

    list_details_handler();

    // ---------------------- Index Page -------------------- //
    $('#index-select').click(function(){
        var This = $(this);
        var url = This.data('url');
        var data = 'id=' + $('#data-list').val();

        $.ajax({
            url: url,
            type: 'post',
            data: data,

            success: function(data){
                var container = $('#index-list').children('.row');
                container.children('div').remove();
                $(data).insertBefore(This);
                list_details_handler();
            }
        });
    });

    var successMessage = $('#success-message');
    var errorMessage = $('#error-message');

    $('#index-create').click(function(e){
        e.preventDefault();
        var This = $(this);
        var url = This.data('url');
        var data = This.parent('form').serialize();
        $.ajax({
            url: url,
            type: 'post',
            data: data,

            success: function(data){
                var container = $('#index-list').children('.row');
                container.children('div').remove();
                $(data).insertBefore($('#index-select'));
                list_details_handler();
                successMessage.show();
                setTimeout(function(){successMessage.hide();}, 4000);
            },

            error: function(data){
                errorMessage.show();
                setTimeout(function(){errorMessage.hide();}, 4000);
            },

            complete: function(){
                This.removeAttr('disabled');
            }
        });
        This.attr('disabled', 'disabled');
    });

    // ---------------------- Crawl Page -------------------- //
    $('#clear-dataset').click(function(){
        var This = $(this);
        var url = This.data('url');
        $.ajax({
            url: url,
            type: 'post',

            success: function(data){
                var row = This.parent('.row');
                var section = row.parent();
                row.remove();
                section.append($(data));
            }
        });
        This.attr('disabled', 'disabled');
    });

    $('#crawl-start').click(function(e){
        e.preventDefault();
        var This = $(this);
        var url = This.data('url');
        var data = This.parent('form').serialize();
        $.ajax({
            url: url,
            type: 'post',
            data: data,

            success: function(data){
                if(data.OK)
                {
                    $('#progress-box').show();
                    window.progressBar = $('#progress-bar');
                    window.progressText = $('#progress-text');
                    window.progressUrl = This.data('progress-url');
                    window.progressAjax = setInterval(requestProgress, 500);
                    This.remove();
                }
            }
        });
    });

});

function list_details_handler(){
    $('#data-list').change(function(){
        var id = $(this).val();
        $('.data-details').hide().filter('[data-id=' + id + ']').show();
    });
}

function requestProgress(){
    $.ajax({
        url: progressUrl,
        type: 'post',

        success: function(data){
            if(data.state == 'running')
            {
                progressText.text(data.percent + " (" + data.docs + " docs)");
                progressBar.width(data.percent);
            }
            else
            {
                clearInterval(progressAjax);
                progressBar.width('100%');
                progressText.text('Crawl done! ' + data.docs + ' documents added to dataset.');
            }
        },

        error: function(data){
            clearInterval(progressAjax);
        }
    });
}