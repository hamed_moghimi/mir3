# Create your views here.
import os
from django.shortcuts import render
from search.crawler import Crawler
from search.models import Crawl, Index
from ui.funcs import sizefmt, get_size


def homePage(request):
    return render(request, 'home.html', {})


def crawlPage(request):

    crawls = Crawl.objects.order_by('-datetime')
    corpus = Crawler.corpus
    docs = len(os.listdir(corpus))
    space = sizefmt(get_size(corpus))

    return render(request, 'crawl.html', {'crawls': crawls, 'docs': docs, 'space': space})


def indexPage(request):
    indices = Index.objects.all()
    return render(request, 'index.html', {'indices': indices})

