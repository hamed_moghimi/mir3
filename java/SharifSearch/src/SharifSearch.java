import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.search.ScoreDoc;


public class SharifSearch {
	public static void main(String[] args) {
//		System.out.println(System.getProperty("user.dir"));
		SearchEngine s = new SearchEngine();
		if(args.length == 6 && args[0].equals("search")){
			/* 
			 * args[0] = search
			 * args[1] = index name
			 * args[2] = use stem? write true or false
			 * args[3] = start
			 * args[4] = end
			 */
			try{
//				Scanner scanner = new Scanner(System.in);
				Result[] res = s.search(args[1], args[5], Boolean.parseBoolean(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]));
				System.out.print(res.length + " ");
				for(int i = 0 ; i < res.length ; i++){
					System.out.print(res[i].id + " ");
				}
				System.out.println();
			}
			catch(Exception e){
				e.printStackTrace();
				System.err.println("illegal argument format 1");
			}
		}
		else if(args.length == 4 && args[0].equals("index")){
			/*
			 * args[0] = index
			 * args[1] = corpus
			 * args[2] = index name
			 * args[3] = 000 pagerank - stem - stopword 
			 */
			if(args[3].length() != 3){
				System.err.println("illegal argument format 2");
				return;
			}
			boolean pr = (args[3].charAt(0) == '0') ? false : true;
			boolean stem = (args[3].charAt(1) == '0') ? false : true;
			boolean stop = (args[3].charAt(2) == '0') ? false : true;
			s.index(args[1], args[2], pr, stem, stop);
		}
		else{
			System.err.println("illegal arguments 3");
			return;
		}
		
//		s.index("docs", "testIndex", false, true, false);
//		
//		Result[] res = s.search("testIndex", "http://en.wikipedia.org/wiki/Information_retrieval", false, 1, 30);
//		for(Result r : res){
//			System.out.println(r);
//		}
	}
}
