import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.DisjunctionMaxQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopFieldDocs;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.spans.SpanNearQuery;
import org.apache.lucene.search.spans.SpanQuery;
import org.apache.lucene.search.spans.SpanTermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.apache.lucene.util.Version;

import com.google.gson.Gson;

public class SearchEngine {
	
	private MMapDirectory index;
	private IndexWriter writer;
	private HashMap<String, Float> pageRankMap = new HashMap<String, Float>();;
	
	private final float titleBoost = 10.0f;
	private final float headersBoost = 2.0f; //1
	private final float textBoost = 5.0f; //5
	private final float urlBoost = 5.0f;
	
	private boolean inc_pr;
	
	private final float phraseBoost = 5.0f;
	private final float spanBoost = 3.0f;
	private final float defBoost = 1.0f;
	private final float queryTitleBoost = 5.0f;
	private final float queryHeadersBoost = 2.0f; //2
	private final float querydefBoost = 3.0f; //3
	private final int bodyProximity = 2; 
	private final int titleProximity = 0;
	private final int headerProximity = 1;
	private final float tieBreaker = 0.1f;
	
	public SearchEngine(){
		
	}
	
	public void index(String dir, String indexDir, boolean inc_pr, boolean inc_stopword, boolean inc_stem){
		this.inc_pr = inc_pr;
		Analyzer analyzer = getAnalyzer(inc_stem, inc_stopword);
		try {
			File dirExists = null;
			File curDir = new File(".");
			if(curDir.isDirectory()){
				String[] files = curDir.list();
				for(String f : files){
					File tempF = new File(curDir, f);
					if(tempF.isDirectory() && tempF.getName().equals(indexDir)){
						dirExists = tempF;
						break;
					}
				}
			}
			if(dirExists != null){
				deleteDirectory(dirExists);
			}
			
			index = new MMapDirectory(new File(curDir, indexDir));
			
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_46, analyzer);
			writer = new IndexWriter(index, config);
			
			File corpusPath = new File(dir);
			
			Scanner pageRankScanner = new Scanner(new File(dir, ".pagerank"));
			while(pageRankScanner.hasNext()){
				pageRankMap.put(pageRankScanner.next(), pageRankScanner.nextFloat());
			}
			System.out.println("PageRank size : " + pageRankMap.size());
			pageRankScanner.close();
			indexDirectory(corpusPath);
			writer.close();
		} catch (IOException e) {
			System.err.println("Error while creating the index, contact the admin");
			e.printStackTrace();
			return;
		}
		finally{
			if(new File(indexDir).exists()){
//				System.out.println("HElloooooooooooooo");
			}
			analyzer.close();
			if(index != null){
				index.close();
			}
		}
	}
	
	private void indexDirectory(File f){
		if(f.isFile()){
			if(f.getName().startsWith(".")){
				return;
			}
			Scanner s;
			try {
				s = new Scanner(f);
			} catch (FileNotFoundException e) {
				System.err.println("File " + f.getPath() + " does not exist");
				return;
			}
			String content = "";
			while(s.hasNext()){
				content += s.nextLine() + "\n";
			}
			s.close();
			
			Gson gson = new Gson();
			Reader reader = new InputStreamReader(new ByteArrayInputStream(content.getBytes()));
			
			SharifDoc doc = gson.fromJson(reader, SharifDoc.class);
			SharifDoc defDoc = SharifDoc.getDefDoc();
			if(doc == null){
				doc = defDoc;
				System.out.println("DOC IS NULLLLL");
			}
			float pageRank;
			try{
				if(doc.id == null){
					pageRank = 0.0f;
				}
//				int docId = Integer.parseInt(doc.id);
				if(pageRankMap.get(doc.id) == null){
					System.out.println("HEREEEEE 222");
					pageRank = 0.0f;
				}
				else{
					pageRank = inc_pr ? pageRankMap.get(doc.id) : 1.0f;
				}
			}
			catch(Exception e){
				e.printStackTrace();
				System.out.println("HEREEEEEE");
				pageRank = 0.0f;
			}
			
			Document indexDoc = new Document();
		
			//id
			StringField idField = new StringField("id", (doc.id == null ? defDoc.id : doc.id), Field.Store.YES);
//			idField.setBoost(0.0f);
			indexDoc.add(idField);
			
			//url
			StringField urlFiled = new StringField("url", (doc.url == null ? defDoc.url : doc.url), Field.Store.YES);
//			urlFiled.setBoost(pageRank * urlBoost);
			indexDoc.add(urlFiled);
			
			//text
			TextField tField = new TextField("text", (doc.text == null ? defDoc.text : doc.text), Field.Store.YES);
			tField.setBoost(pageRank * textBoost);
			indexDoc.add(tField);
			
			//title
			TextField titleField = new TextField("title", (doc.title == null ? defDoc.title : doc.title), Field.Store.YES);
			titleField.setBoost(pageRank * titleBoost);
			indexDoc.add(titleField);
			
			//headers
			TextField headerField = new TextField("headers", (doc.headers == null ? defDoc.headers : doc.headers), Field.Store.YES);
			headerField.setBoost(pageRank * headersBoost);
			indexDoc.add(headerField);
			
			
			try {
				writer.updateDocument(new Term("id", (doc.id == null ? defDoc.id : doc.id)), indexDoc);
			} catch (IOException e) {
				System.err.println("Error while adding file " + f.getPath() + " to index");
			}
		}
		else if(f.isDirectory()){
			String[] filenameList = f.list();
			for(String s : filenameList){
				indexDirectory(new File(f, s));
				//System.out.println(s);
			}
		}
	}
	
	public Result[] search(String indexDir, String query, boolean inc_stem, int start, int end){
		 //(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]
		Pattern p = Pattern.compile("((https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])");
		String original = query;
		query = query.replaceAll("((https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])", "");
//		System.out.println("Query is " + query);
		start--;
		end--;
		File indexPath = new File(indexDir);
		
		MMapDirectory index = null;
		IndexReader reader = null;
		try{
			index = new MMapDirectory(indexPath);
			reader = DirectoryReader.open(index);
		}
		catch(Exception e){
			System.err.println("Error occured while opening index.");
			e.printStackTrace();
			return new Result[0];
		}
		
		List<String> allTerms = getQueryTerms(query, inc_stem, false);
		List<String> terms = getQueryTerms(query, inc_stem, true);
		Map<String, Float> counts = getCountsMap(terms);
		
		DisjunctionMaxQuery textQuery = getMaxQuery(counts, allTerms, "text", bodyProximity, reader);
		textQuery.setBoost(querydefBoost);
		DisjunctionMaxQuery titleQuery = getMaxQuery(counts, allTerms, "title", titleProximity, reader);
		titleQuery.setBoost(queryTitleBoost);
		DisjunctionMaxQuery headerQuery = getMaxQuery(counts, allTerms, "headers", headerProximity, reader);
		headerQuery.setBoost(queryHeadersBoost);
		
		
		Matcher m = p.matcher(original);
		List<String> urls = new ArrayList<String>();
		while(m.find()){
			urls.add(m.group(1).toLowerCase().substring(m.group(1).indexOf(":") + 3));
		}
		BooleanQuery finalURLQuery = new BooleanQuery();
		for(String u : urls){
			finalURLQuery.add(getURLQuery(u, "url"), Occur.SHOULD);
//			BooleanQuery urlQuery = getURLQuery(query, "url");
		}
		finalURLQuery.setBoost(urlBoost);
		
		BooleanQuery finalQuery = new BooleanQuery();
		finalQuery.add(textQuery, Occur.SHOULD);
		finalQuery.add(titleQuery, Occur.SHOULD);
		finalQuery.add(headerQuery, Occur.SHOULD);
		finalQuery.add(finalURLQuery, Occur.SHOULD);
		
//		System.out.println(finalQuery);
		ArrayList<Result> res = new ArrayList<Result>();
		ScoreDoc[] hits = null;
		try{
			IndexSearcher searcher = new IndexSearcher(reader);
			TopDocs docs = searcher.search(finalQuery, end);
			hits = docs.scoreDocs;
			for(int i = 0 ; i < hits.length ; i++){
				String id = searcher.doc(hits[i].doc).get("id");
				String title = searcher.doc(hits[i].doc).get("title");
				res.add(new Result(title, id, hits[i].score));
			}
		}
		catch(Exception e){
			System.err.println("Exception occured while searching.");
		}
		Result[] finalRes = new Result[res.size()];
//		Result[] finalRes = hits;
		for(int i = 0 ; i < res.size() ; i++){
			finalRes[i] = res.get(i);
		}
		if(finalRes.length < start + 1){
			return new Result[0];
		}
		if(finalRes.length <= end + 1){
			return Arrays.copyOfRange(finalRes, start, finalRes.length);
		}
		return Arrays.copyOfRange(finalRes, start, end + 1);
	}
	
	private List<String> getQueryTerms(String query, boolean inc_stem, boolean inc_stopword){
		List<String> result = new ArrayList<String>();
		Analyzer analyzer = getAnalyzer(inc_stem, inc_stopword);
		try {
	      TokenStream stream  = analyzer.tokenStream(null, new StringReader(query));
	      stream.reset();
	      while (stream.incrementToken()) {
	        result.add(stream.getAttribute(CharTermAttribute.class).toString());
	      }
	    } catch (IOException e) {
	      // not thrown b/c we're using a string reader...
	    	result = new ArrayList<String>();
	    }
	    finally{
	    	analyzer.close();
	    }
	    return result;
	}
	
	private Map<String, Float> getCountsMap(List<String> terms){
		Map<String, Float> res = new HashMap<String, Float>();
		for(String st : terms){
			if(res.get(st) == null){
				res.put(st, 1.0f);
			}
			else{
				res.put(st, res.get(st) + 1); 
			}
		}
		for(Entry<String, Float> ent : res.entrySet()){
			res.put(ent.getKey(), 1 + (float)Math.log10(ent.getValue()));
		}
		return res;
	}
	
	private PhraseQuery getPhraseQuery(List<String> terms, String fieldName){
		PhraseQuery res = new PhraseQuery();
		for(String st: terms){
			res.add(new Term(fieldName, st));
		}
		res.setBoost(phraseBoost);
		return res;
	}
	
	private SpanNearQuery getSpanQuery(Map<String, Float> terms, String fieldName, int dist){
		ArrayList<SpanQuery> termSpans = new ArrayList<SpanQuery>();
		for(String st : terms.keySet()){
			termSpans.add(new SpanTermQuery(new Term(fieldName, st)));
		}
		SpanQuery[] queryArray = new SpanQuery[termSpans.size()];
		queryArray = termSpans.toArray(queryArray);
		SpanNearQuery res = new SpanNearQuery(queryArray, dist, false);
		res.setBoost(spanBoost);
		return res;
	}
	
	private BooleanQuery getBooleanQuery(Map<String, Float> terms, String fieldName, IndexReader reader){
		BooleanQuery res = new BooleanQuery();
		int totalCount = reader.maxDoc();
		for(Entry<String,Float> ent : terms.entrySet()){
			TermQuery tq = new TermQuery(new Term(fieldName, ent.getKey()));
			int colFreq = 0;
			try {
				colFreq = reader.docFreq(new Term("text", ent.getKey()));
			} catch (IOException e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
			}
			float idf = (colFreq == 0) ? 1 : 1 + (float)Math.log10(((double)colFreq) / totalCount); 
			tq.setBoost(ent.getValue() * idf);
			res.add(tq, Occur.SHOULD);
		}
		res.setBoost(defBoost);
		return res;
	}
	
	private Analyzer getAnalyzer(boolean inc_stem, boolean inc_stopword){
		Analyzer analyzer = new SimpleAnalyzer(Version.LUCENE_46);
		CharArraySet stopwords = inc_stopword ? StopAnalyzer.ENGLISH_STOP_WORDS_SET : CharArraySet.EMPTY_SET;
		if(inc_stem){
			/* A Analyzer.TokenStreamComponents built from an StandardTokenizer filtered with StandardFilter, 
			 * EnglishPossessiveFilter, LowerCaseFilter, StopFilter , KeywordMarkerFilter 
			 * if a stem exclusion set is provided and PorterStemFilter.
			 */
			analyzer = new EnglishAnalyzer(Version.LUCENE_46, stopwords);
		}
		else{
			//Filters StandardTokenizer with StandardFilter, LowerCaseFilter and StopFilter
			analyzer = new StandardAnalyzer(Version.LUCENE_46, stopwords);
		}
		return analyzer;
	}
	
	private DisjunctionMaxQuery getMaxQuery(Map<String, Float> terms, List<String> allTerms, String fieldName, int dist, IndexReader reader){
		DisjunctionMaxQuery res = new DisjunctionMaxQuery(tieBreaker);
		res.add(getPhraseQuery(allTerms, fieldName));
		res.add(getSpanQuery(terms, fieldName, dist));
		res.add(getBooleanQuery(terms, fieldName, reader));
		return res;
	}
	
	private BooleanQuery getURLQuery(String query, String fieldName){
		String[] terms = query.trim().split(" ");
		BooleanQuery res = new BooleanQuery();
		for(String st : terms){
			if(st.contains(".")){
				TermQuery q = new TermQuery(new Term(fieldName, st.toLowerCase()));
				res.add(q, Occur.SHOULD);
			}
		}
		return res;
	}
	
	private void deleteDirectory(File dir){
		if(dir.isDirectory()){
			String[] files = dir.list();
			for(String file : files){
				new File(dir, file).delete();
			}
		}
		dir.delete();
	}
}

class Result{
	String title;
	String id;
	float score;
	
	public Result(String title, String id, float score){
		this.title = title;
		this.id = id;
		this.score = score;
	}
	
	public String toString(){
		return title + " : " + id + " - " + score;
	}
}