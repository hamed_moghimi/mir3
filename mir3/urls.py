from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # admin
    url(r'^admin/', include(admin.site.urls)),

    # home page
    url(r'^$', 'ui.views.homePage', name='home'),

    # search result page
    url(r'^search$', 'search.views.search', name='search'),

    # crawl page, starts crawl, crawl progress and clear current dataset
    url(r'^crawl$', 'ui.views.crawlPage', name='crawl'),
    url(r'^crawl/start$', 'search.views.crawl_start', name='crawl-start'),
    url(r'^crawl/progress$', 'search.views.crawl_progress', name='crawl-progress'),
    url(r'^clear$', 'search.views.clear', name='clear'),

    # index page
    url(r'^index$', 'ui.views.indexPage', name='index'),
    url(r'^index/select$', 'search.views.index_select', name='index-select'),
    url(r'^index/create', 'search.views.index_create', name='index-create'),
)
